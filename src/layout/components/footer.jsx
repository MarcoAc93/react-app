import React from 'react';
import { Segment, Header } from 'semantic-ui-react';

const FooterComponent = () => {
  return (
    <Segment>
      <Header as="h1">Footer</Header>
    </Segment>
  );
};

export default FooterComponent;
