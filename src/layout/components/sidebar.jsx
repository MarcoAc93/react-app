import React from 'react';
import { Sidebar, Segment, Menu } from 'semantic-ui-react';
import MainMenu from './main-menu';
import { BrowserRouter as Router } from 'react-router-dom';

class SidebarComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { }
  }

  render() {
    const { children } = this.props;
    return(
      <div>
        <Router>
          <Sidebar.Pushable as={Segment}>
            <Sidebar as={Menu} animation="slide along" width="thin" visible={true} icon="labeled" vertical>
              <MainMenu />
            </Sidebar>
            <Sidebar.Pusher>
              <Segment style={{ height: '39.6rem' }}>
                  {children()}
              </Segment>
            </Sidebar.Pusher>
          </Sidebar.Pushable>
        </Router>
      </div>
    )
  }
}

export default SidebarComponent;
