import React from 'react';
import { Segment, Header } from 'semantic-ui-react';

const HeaderComponent = () => {
  return (
    <Segment>
      <Header as="h1">React-App</Header>
    </Segment>
  );
};

export default HeaderComponent;
