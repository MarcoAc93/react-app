import React from 'react';
import { Menu, Icon } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom'

class MainMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: ''
    };
  }

  handleItemClick = (event, { name }) => {
    const { history } = this.props;
    this.setState({ activeItem: name })
    history.push(`/${name}`);
  }

  itemStyle = () => ({
    height: '3rem',
    fontSize: '1.5rem',
    textAlign: 'left',
    paddingLeft: '.5rem'
  })

  render() {
    const { activeItem } = this.state;
    return (
      <Menu vertical style={{ height: '39.49rem', width: '10.6rem' }}>
        <div>
          <Menu.Item style={this.itemStyle()} name='home' active={activeItem === 'inbox'} onClick={this.handleItemClick}>
            <span><Icon name="home" /></span> Home
          </Menu.Item>
          <Menu.Item style={this.itemStyle()} name='contact' active={activeItem === 'spam'} onClick={this.handleItemClick}>
            <span><Icon name="address card" /></span> Contact
          </Menu.Item>
          <Menu.Item style={this.itemStyle()} name='about' active={activeItem === 'updates'} onClick={this.handleItemClick}>
            <span><Icon name="info circle" /></span> About
          </Menu.Item>
        </div>
      </Menu>
    );
  }
};

export default withRouter(MainMenu);