import React from 'react';
import Header from './components/header';
import Sidebar from './components/sidebar';
import Footer from './components/footer';
import AppRoutes from '../routes';

class Layout extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Sidebar children={AppRoutes} />
        <Footer />
      </div>
    )
  }
};

export default Layout;
