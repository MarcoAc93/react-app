import React from 'react';
import { render } from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import Layout from './layout';

render(
  <Layout />,
  document.getElementById('root')
);
