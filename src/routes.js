import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './pages/home';
import About from './pages/about';
import Contact from './pages/contact';
import page404 from './pages/page404';

const AppRoutes = () => (
  <Switch>
    <Route path="/home" component={Home} />
    <Route path="/about" exact component={About} />
    <Route path="/contact" exact component={Contact} />
    <Route path="/" exact component={Home} />
    <Route component={page404} />
  </Switch>
);

export default AppRoutes;
